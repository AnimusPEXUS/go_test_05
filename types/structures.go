package types

type OutError struct {
	Message string `json:"message"`
}

type InMessageNewTask struct {
	Payload       string `json:"payload"`
	HashRoundsCnt uint   `json:"hash_rounds_cnt"`
}

type OutMessageNewTaskOkResp struct {
	Id uint `json:"id"`
}

type InMessageGetStatus struct {
	Id uint `json:"id"`
}

type OutMessageGetStatusOkResp struct {
	Id            uint   `json:"id"`
	Payload       string `json:"payload"`
	HashRoundsCnt uint   `json:"hash_rounds_cnt"`
	Status        string `json:"status"`
	Hash          string `json:"hash"`
}

type DBRecord struct {
	Id            uint `gorm:"PRIMARY_KEY"`
	Payload       []byte
	HashRoundsCnt uint
	Status        string
	Hash          string
}
