package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/AnimusPEXUS/go_test_05/types"
)

func main() {

	host := flag.String("host", "localhost", "host")
	port := flag.String("port", "3030", "port")

	flag.Parse()

	msg := &types.InMessageNewTask{
		Payload:       "123123123123",
		HashRoundsCnt: 3,
	}

	data, err := json.Marshal(msg)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("sending:")
	log.Println(data)

	reader := bytes.NewReader(data)

	resp, err := http.Post(fmt.Sprintf("http://%s:%s/add", *host, *port), "application/json", reader)

	log.Println("response is:")
	log.Println(resp.StatusCode)

	b := &bytes.Buffer{}

	io.Copy(b, resp.Body)

	resp0 := &types.OutMessageNewTaskOkResp{}

	err = json.Unmarshal(b.Bytes(), resp0)

	if err != nil {
		log.Fatalln("resp 0 unmarshalling error:", err)
	}

	log.Println("resp id:", resp0.Id)

	for _ = range [5]struct{}{} {
		time.Sleep(1 * time.Second)
		log.Println("tick..")
	}

	msg2 := &types.InMessageGetStatus{
		Id: resp0.Id,
	}

	data, err = json.Marshal(msg2)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("sending:")
	log.Println(data)

	reader = bytes.NewReader(data)

	resp, err = http.Post(fmt.Sprintf("http://%s:%s/status", *host, *port), "application/json", reader)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("response is:")
	log.Println(resp.StatusCode)

	io.Copy(os.Stdout, resp.Body)

	return
}
