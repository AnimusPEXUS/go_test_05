package main

import (
	"github.com/jinzhu/gorm"

	"bitbucket.org/AnimusPEXUS/go_test_05/types"
)

type DBController struct {
	db *gorm.DB
}

func NewDBController(db *gorm.DB) *DBController {
	self := &DBController{
		db: db,
	}
	db.AutoMigrate(&types.DBRecord{})
	return self
}

func (self *DBController) CreateTask(payload []byte, hash_rounds_cnt uint) (uint, error) {
	r := &types.DBRecord{
		Payload:       payload,
		HashRoundsCnt: hash_rounds_cnt,
		Status:        "in progress",
		Hash:          "",
	}

	err := self.db.Create(r).Error
	if err != nil {
		return 0, err
	}

	return r.Id, nil
}

func (self *DBController) SetStatusAndHash(id uint, status string, hash string) error {
	err := self.db.Model(&types.DBRecord{}).
		Where("id = ?", id).
		Updates(map[string]interface{}{"status": status, "hash": hash}).Error
	if err != nil {
		return err
	}
	return nil
}

func (self *DBController) GetRecord(id uint) (*types.DBRecord, error) {
	var ret types.DBRecord
	err := self.db.Where("id = ?", id).Take(&ret).Error
	if err != nil {
		return nil, err
	}
	return &ret, nil
}
