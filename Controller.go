package main

import (
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"log"

	"github.com/valyala/fasthttp"

	"bitbucket.org/AnimusPEXUS/go_test_05/types"
)

type Controller struct {
	db *DBController
}

func NewController(dbcontroller *DBController) *Controller {
	self := &Controller{
		db: dbcontroller,
	}
	return self
}

func (self *Controller) Add(ctx *fasthttp.RequestCtx) {

	var in_msg types.InMessageNewTask

	body := ctx.PostBody()

	err := json.Unmarshal(body, &in_msg)
	if err != nil {
		log.Println(err)
		return
	}

	pl_b, err := base64.StdEncoding.DecodeString(in_msg.Payload)
	if err != nil {
		ReportError(ctx, "payload must be base64 encoded: "+err.Error())
		return
	}

	id, err := self.db.CreateTask(pl_b, in_msg.HashRoundsCnt)
	if err != nil {
		ReportError(ctx, "error creating new task: "+err.Error())
		return
	}

	resp_data, err := json.Marshal(&types.OutMessageNewTaskOkResp{Id: id})
	if err != nil {
		ReportError(ctx, "error creating new task ok response: "+err.Error())
		return
	}

	ctx.SetStatusCode(200)
	ctx.Write(resp_data)

	go self.TaskWorker(id)

}

func (self *Controller) Status(ctx *fasthttp.RequestCtx) {

	var (
		err       error
		resp_data []byte
	)

	{

		var in_msg types.InMessageGetStatus

		{
			body := ctx.PostBody()

			err := json.Unmarshal(body, &in_msg)
			if err != nil {
				log.Println(err)
				return
			}
		}

		var r *types.DBRecord

		r, err = self.db.GetRecord(in_msg.Id)
		if err != nil {
			ReportError(ctx, "error getting status of task: "+err.Error())
			return
		}

		ret := &types.OutMessageGetStatusOkResp{
			Id:            r.Id,
			Payload:       base64.StdEncoding.EncodeToString(r.Payload),
			HashRoundsCnt: r.HashRoundsCnt,
			Status:        r.Status,
			Hash:          "",
		}

		if r.Status == "finished" {
			ret.Hash = r.Hash
		}

		resp_data, err = json.Marshal(ret)
		if err != nil {
			ReportError(ctx, "error creating new task ok response: "+err.Error())
			return
		}
	}

	ctx.SetStatusCode(200)
	ctx.Write(resp_data)

}

func (self *Controller) TaskWorker(id uint) {

	r, err := self.db.GetRecord(id)
	if err != nil {
		log.Printf("error hashing data at record id %d", id)
		return
	}

	data := make([]byte, len(r.Payload))
	copy(data, r.Payload)

	for i := uint(0); i != r.HashRoundsCnt; i++ {
		h := sha512.New()
		h.Write(data)
		data = h.Sum([]byte{})
	}

	h := hex.EncodeToString(data)

	self.db.SetStatusAndHash(id, "finished", h)

}

func RenderErrorData(message string) ([]byte, error) {
	ret, err := json.Marshal(&types.OutError{Message: message})
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func ReportError(ctx *fasthttp.RequestCtx, message string) {
	ctx.SetStatusCode(500)

	data, err := RenderErrorData(message)
	if err == nil {
		ctx.Write(data)
	}
	return
}
