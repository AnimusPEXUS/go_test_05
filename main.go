package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

func main() {

	var (
		DBMS_HOST     = "db"
		DBMS_PORT     = "5432"
		DBMS_USER     = "postgres"
		DBMS_PASSWORD = "example"
		DBMS_DB_NAME  = "postgres"

		HOST = "0.0.0.0:3030"
	)

	parameters := []struct {
		P *string
		T string
	}{
		{&DBMS_HOST, "DBMS_HOST"},
		{&DBMS_PORT, "DBMS_PORT"},
		{&DBMS_USER, "DBMS_USER"},
		{&DBMS_PASSWORD, "DBMS_PASSWORD"},
		{&DBMS_DB_NAME, "DBMS_DB_NAME"},
		{&HOST, "HOST"},
	}

	for _, i := range os.Environ() {

		splitted := strings.SplitN(i, "=", 2)

		for _, j := range parameters {

			if splitted[0] == j.T {
				*j.P = splitted[1]
			}

		}

	}

	for _, i := range parameters {
		log.Printf("%20s: %s\n", i.T, *i.P)
	}

	db, err := gorm.Open(
		"postgres",
		fmt.Sprintf(
			"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			DBMS_HOST,
			DBMS_PORT,
			DBMS_USER,
			DBMS_PASSWORD,
			DBMS_DB_NAME,
		),
	)
	if err != nil {
		log.Fatalln(err)
	}

	db_ctrlr := NewDBController(db)

	ctrlr := NewController(db_ctrlr)

	r := fasthttprouter.New()

	r.POST("/add", ctrlr.Add)
	r.POST("/status", ctrlr.Status)

	server := &fasthttp.Server{Handler: r.Handler}

	err = server.ListenAndServe(HOST)
	if err != nil {
		log.Fatalln(err)
	}

	os.Exit(0)
}
